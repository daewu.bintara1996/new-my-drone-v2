package com.satmei.mydrone.modules.dashboard;


import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.satmei.mydrone.R;
import com.satmei.mydrone.base.BaseActivity;
import com.satmei.mydrone.modules.login.LoginActivity;
import com.satmei.mydrone.modules.splash.SplashActivity;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;

public class DashboardActivity extends BaseActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private int DELAY_UPDATE = 2000;
    double accelerometerX, accelerometerY, accelerometerZ;
    double gyroscopeX, gyroscopeY, gyroscopeZ;
    double magneticFieldX, magneticFieldY, magneticFieldZ;
    private static DecimalFormat df3 = new DecimalFormat("#.###");
    Timer timer = new Timer();


    TextView tvAx, tvAy, tvAz, tvGx, tvGy, tvGz, tvMFx, tvMFy, tvMFz, tvEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        tvAx = findViewById(R.id.tvAx);
        tvAy = findViewById(R.id.tvAy);
        tvAz = findViewById(R.id.tvAz);
        tvGx = findViewById(R.id.tvGx);
        tvGy = findViewById(R.id.tvGy);
        tvGz = findViewById(R.id.tvGz);
        tvMFx = findViewById(R.id.tvMFx);
        tvMFy = findViewById(R.id.tvMFy);
        tvMFz = findViewById(R.id.tvMFz);
        tvEmail = findViewById(R.id.tvEmail);

        tvAx.setText("X : ");
        tvAy.setText("Y : ");
        tvAz.setText("Z : ");
        tvGx.setText("X : ");
        tvGy.setText("Y : ");
        tvGz.setText("Z : ");
        tvMFx.setText("X : ");
        tvMFy.setText("Y : ");
        tvMFz.setText("Z : ");

        tvEmail.setText(myPreferences.getPreferencesString(TOKEN_EMAIL_KEY));

        sensorManager=(SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL);

        onSensorReadyToUpdate();

    }

    private void onSensorReadyToUpdate() {
        timer.scheduleAtFixedRate(new TimerTask() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {


                tvAx.setText("X : " + String.valueOf(df3.format(accelerometerX)));
                tvAy.setText("Y : " + String.valueOf(df3.format(accelerometerY)));
                tvAz.setText("Z : " + String.valueOf(df3.format(accelerometerZ)));
                tvGx.setText("X : " + String.valueOf(df3.format(gyroscopeX)));
                tvGy.setText("Y : " + String.valueOf(df3.format(gyroscopeY)));
                tvGz.setText("Z : " + String.valueOf(df3.format(gyroscopeZ)));
                tvMFx.setText("X : " + String.valueOf(df3.format(magneticFieldX)));
                tvMFy.setText("Y : " + String.valueOf(df3.format(magneticFieldY)));
                tvMFz.setText("Z : " + String.valueOf(df3.format(magneticFieldZ)));

                onSensorUpdate();

            }
        },DELAY_UPDATE, DELAY_UPDATE);
    }

    private void onSensorUpdate() {
        String USER_ID = myPreferences.getPreferencesString(TOKEN_USER_ID_KEY);

        if (USER_ID == null){
            myPreferences.clearAllPreferences();
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        myRef = database.getReference("user-sensors");

        // ACCELEROMETER
        myRef.child(USER_ID).child("accelerometer").child("X").setValue(String.valueOf(df3.format(accelerometerX)));
        myRef.child(USER_ID).child("accelerometer").child("Y").setValue(String.valueOf(df3.format(accelerometerY)));
        myRef.child(USER_ID).child("accelerometer").child("Z").setValue(String.valueOf(df3.format(accelerometerZ)));

        // GYROSCOPE
        myRef.child(USER_ID).child("gyroscope").child("X").setValue(String.valueOf(df3.format(gyroscopeX)));
        myRef.child(USER_ID).child("gyroscope").child("Y").setValue(String.valueOf(df3.format(gyroscopeY)));
        myRef.child(USER_ID).child("gyroscope").child("Z").setValue(String.valueOf(df3.format(gyroscopeZ)));

        // MAGNETIC FIELD
        myRef.child(USER_ID).child("magneticField").child("X").setValue(String.valueOf(df3.format(magneticFieldX)));
        myRef.child(USER_ID).child("magneticField").child("Y").setValue(String.valueOf(df3.format(magneticFieldY)));
        myRef.child(USER_ID).child("magneticField").child("Z").setValue(String.valueOf(df3.format(magneticFieldZ)));
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
            accelerometerX =event.values[0];
            accelerometerY =event.values[1];
            accelerometerZ =event.values[2];
        }
        if (event.sensor.getType()==Sensor.TYPE_GYROSCOPE){
            gyroscopeX =event.values[0];
            gyroscopeY =event.values[1];
            gyroscopeZ =event.values[2];
        }
        if (event.sensor.getType()==Sensor.TYPE_MAGNETIC_FIELD){
            magneticFieldX =event.values[0];
            magneticFieldY =event.values[1];
            magneticFieldZ =event.values[2];
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onDestroy() {
        timer.cancel();
        super.onDestroy();
    }

    public void btnLogout(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Keluar Akun");
        builder.setMessage("Yakin akan keluar dari akun ini?");
        builder.setPositiveButton("Keluar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                timer.cancel();
                myPreferences.clearAllPreferences();
                myPreferences.clearAllPreferences();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}