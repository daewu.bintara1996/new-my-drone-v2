package com.satmei.mydrone.utils;

import android.content.Context;
import android.widget.Toast;

import com.satmei.mydrone.modules.splash.SplashActivity;

public class MyToast {
    Context context;
    public MyToast(Context context){
        this.context = context;
    }

    public void showShortToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void showLongToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

}
